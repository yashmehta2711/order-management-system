
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.MessageFormat;
import java.awt.print.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import net.proteanit.sql.DbUtils;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author yashm
 */
public class HomePage extends javax.swing.JPanel {

    /**
     * Creates new form HomePage
     */
    Connection conn = null;
    Statement statement = null;
    PreparedStatement ps = null;
    ResultSet rs = null;
    HomePage hp;
    String sent = "No";
    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");  
    LocalDateTime now = LocalDateTime.now(); 
    public HomePage() {
        initComponents();
        this.hp = this;
        conn = MySQLConnect.connectDB();
        String sql = "select customer_order.customer_id, customer_order.order_id, customer_order.mj_id, customer_order.receive_date, customer_order.delivery_date, gs_order.gs_name, gs_order.given_date, gs_order.receive_date, gs_order.received from gs_order join customer_order on gs_order.mj_id = customer_order.mj_id WHERE customer_order.delivery_date = '" + dtf.format(now) + "'";
        updateTableData(sql);
        txtSearch.getDocument().addDocumentListener(new DocumentListener(){
           @Override
           public void insertUpdate(DocumentEvent e){
               searchFilter();
           }
           @Override
           public void removeUpdate(DocumentEvent e){
               searchFilter();
           }
           @Override
           public void changedUpdate(DocumentEvent e){
           }
           
        });
        
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");  
        LocalDateTime now = LocalDateTime.now();  
//        System.out.println(dtf.format(now));
        String sql1 = "SELECT mj_id,gs_name,given_date, receive_date, received FROM gs_order WHERE receive_date = '" + dtf.format(now) + "'";
        updateTableData1(sql1);
        txtSearch1.getDocument().addDocumentListener(new DocumentListener(){
           @Override
           public void insertUpdate(DocumentEvent e){
               searchFilter1();
           }
           @Override
           public void removeUpdate(DocumentEvent e){
               searchFilter1();
           }
           @Override
           public void changedUpdate(DocumentEvent e){
           }
           
        });
    }
    
    private DefaultTableModel getMyTableModel (TableModel dtm){
        
        int nRow = dtm.getRowCount(), nCol = dtm.getColumnCount();
        Object[][] tableData = new Object[nRow][nCol];
        for(int i=0; i<nRow; i++)
            for(int j=0; j<nCol; j++)
                tableData[i][j] = dtm.getValueAt(i, j);
        String[] colHeads = {"Customer Name","Order ID","MJ ID","Customer Receive Date","Customer Delivery Date","Goldsmith Name","Given Date", "Receive Date", "Received"};
        DefaultTableModel myModel = new DefaultTableModel(tableData, colHeads){
            @Override
            public boolean isCellEditable(int row, int column){
                return false;
            }
        };
        return myModel;
    }
    
    private DefaultTableModel getMyTableModel1 (TableModel dtm){        
        int nRow = dtm.getRowCount(), nCol = dtm.getColumnCount();
        Object[][] tableData = new Object[nRow][nCol];
        for(int i=0; i<nRow; i++)
            for(int j=0; j<nCol; j++)
                tableData[i][j] = dtm.getValueAt(i, j);
        String[] colHeads = {"MJ ID","Goldsmith Name","Given Date", "Receive Date", "Received"};
        DefaultTableModel myModel = new DefaultTableModel(tableData, colHeads){
            @Override
            public boolean isCellEditable(int row, int column){
                return false;
            }
        };
        return myModel;
    }
    
    private void searchFilter(){
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");  
        LocalDateTime now = LocalDateTime.now();  
        String key = txtSearch.getText();
        String sql;
        if(key.equals("")){
                  
            sql = "select customer_order.customer_id, customer_order.order_id, customer_order.mj_id,customer_order.receive_date, customer_order.delivery_date, gs_order.gs_name, gs_order.given_date,gs_order.receive_date, gs_order.received from gs_order join customer_order on gs_order.mj_id = customer_order.mj_id WHERE customer_order.delivery_date = '" + dtf.format(now) + "'";
        }else{
            sql = "select customer_order.customer_id, customer_order.order_id, customer_order.mj_id,customer_order.receive_date,customer_order.delivery_date, gs_order.gs_name, gs_order.given_date,gs_order.receive_date, gs_order.received from gs_order join customer_order on gs_order.mj_id = customer_order.mj_id where customer_order.mj_id like '%"+key+"%' OR customer_order.customer_id like '%"+key+"%' OR customer_order.order_id like '%"+key+"%' AND customer_order.delivery_date = '" + dtf.format(now) + "'";
//            sql = "SELECT customer_id,order_id,mj_id,issue_date FROM customer_order WHERE customer_id like '%"+key+"%'";
        }
        updateTableData(sql);
    }
    
    private void searchFilter1(){
        String key = txtSearch1.getText();
        String sql;
        if(key.equals("")){
            sql = "SELECT mj_id,gs_name,given_date, receive_date, received FROM gs_order WHERE receive_date = '" + dtf.format(now) + "'";
        }else{
            sql = "SELECT mj_id,gs_name,given_date, receive_date, received FROM gs_order WHERE mj_id like '%"+key+"%' OR gs_name like '%"+key+"%' AND receive_date = '" + dtf.format(now) + "'";
        }
        updateTableData1(sql);
    }
    
    private void updateTableData(String sql){
        try{
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            tblC.setModel(getMyTableModel(DbUtils.resultSetToTableModel(rs)));
        }catch(SQLException e){
            JOptionPane.showMessageDialog(this, "Error While Fetching Data from Database!");
        }
//      finally{
//            try{
////                rs.close();
////                ps.close();
//            }catch(SQLException ex){
//                JOptionPane.showMessageDialog(this, "Error While Closing ps or rs!");
//            }
//        }
    }
    
    private void updateTableData1(String sql){
        try{
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            tblGS.setModel(getMyTableModel1(DbUtils.resultSetToTableModel(rs)));
        }catch(SQLException e){
            JOptionPane.showMessageDialog(this, "Error While Fetching Data from Database!");
        }
//        finally{
//            try{
//                rs.close();
//                ps.close();
//            }catch(SQLException ex){
//                JOptionPane.showMessageDialog(this, "Error While Closing ps or rs!");
//            }
//        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlBase = new javax.swing.JPanel();
        pnlC = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtSearch = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblC = new javax.swing.JTable();
        btnPrintC = new javax.swing.JButton();
        pnlGS = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        txtSearch1 = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblGS = new javax.swing.JTable();
        btnPrintGS = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 255));

        pnlBase.setBackground(new java.awt.Color(255, 255, 255));

        pnlC.setBackground(new java.awt.Color(255, 255, 255));
        pnlC.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Customer Report", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Georgia", 1, 24))); // NOI18N

        jLabel1.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        jLabel1.setText("Search");

        txtSearch.setFont(new java.awt.Font("Georgia", 0, 18)); // NOI18N

        tblC.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblC);

        btnPrintC.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        btnPrintC.setText("Print");
        btnPrintC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrintCActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlCLayout = new javax.swing.GroupLayout(pnlC);
        pnlC.setLayout(pnlCLayout);
        pnlCLayout.setHorizontalGroup(
            pnlCLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlCLayout.createSequentialGroup()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtSearch))
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 914, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlCLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnPrintC)
                .addGap(407, 407, 407))
        );
        pnlCLayout.setVerticalGroup(
            pnlCLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlCLayout.createSequentialGroup()
                .addGroup(pnlCLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 243, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnPrintC))
        );

        pnlGS.setBackground(new java.awt.Color(255, 255, 255));
        pnlGS.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Goldsmith Report", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Georgia", 1, 24))); // NOI18N

        jLabel2.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        jLabel2.setText("Search");

        txtSearch1.setFont(new java.awt.Font("Georgia", 0, 18)); // NOI18N

        tblGS.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(tblGS);

        btnPrintGS.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        btnPrintGS.setText("Print");
        btnPrintGS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrintGSActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlGSLayout = new javax.swing.GroupLayout(pnlGS);
        pnlGS.setLayout(pnlGSLayout);
        pnlGSLayout.setHorizontalGroup(
            pnlGSLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlGSLayout.createSequentialGroup()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtSearch1))
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 914, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlGSLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnPrintGS)
                .addGap(392, 392, 392))
        );
        pnlGSLayout.setVerticalGroup(
            pnlGSLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlGSLayout.createSequentialGroup()
                .addGroup(pnlGSLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtSearch1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 380, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnPrintGS))
        );

        javax.swing.GroupLayout pnlBaseLayout = new javax.swing.GroupLayout(pnlBase);
        pnlBase.setLayout(pnlBaseLayout);
        pnlBaseLayout.setHorizontalGroup(
            pnlBaseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlC, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(pnlGS, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        pnlBaseLayout.setVerticalGroup(
            pnlBaseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlBaseLayout.createSequentialGroup()
                .addComponent(pnlC, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlGS, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlBase, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlBase, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnPrintCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrintCActionPerformed
        // TODO add your handling code here:
        MessageFormat header =  new MessageFormat("Customer Report For : " + dtf.format(now));
        MessageFormat footer =  new MessageFormat("Page{0, number, integer}");
        try{
            tblC.print(JTable.PrintMode.FIT_WIDTH, header, footer);
        }catch(Exception e){
            System.err.format("Cannot print %s%n", e.getMessage());
        }
    }//GEN-LAST:event_btnPrintCActionPerformed

    private void btnPrintGSActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrintGSActionPerformed
        // TODO add your handling code here:
        MessageFormat header =  new MessageFormat("Goldsmith Report For : " + dtf.format(now));
        MessageFormat footer =  new MessageFormat("Page{0, number, integer}");
        try{
            tblGS.print(JTable.PrintMode.FIT_WIDTH, header, footer);
        }catch(java.awt.print.PrinterException e){
            System.err.format("Cannot print %s%n", e.getMessage());
        }
    }//GEN-LAST:event_btnPrintGSActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnPrintC;
    private javax.swing.JButton btnPrintGS;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JPanel pnlBase;
    private javax.swing.JPanel pnlC;
    private javax.swing.JPanel pnlGS;
    private javax.swing.JTable tblC;
    private javax.swing.JTable tblGS;
    private javax.swing.JTextField txtSearch;
    private javax.swing.JTextField txtSearch1;
    // End of variables declaration//GEN-END:variables
}
