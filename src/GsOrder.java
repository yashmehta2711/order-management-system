
import com.itextpdf.text.Document;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import net.proteanit.sql.DbUtils;
import java.time.*;
import java.time.format.DateTimeFormatter;
import javax.swing.JTable;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author yashm
 */
public class GsOrder extends javax.swing.JPanel {

    /**
     * Creates new form GsOrder
     */
    Connection conn = null;
    Statement statement = null;
    PreparedStatement ps = null;
    ResultSet rs = null;
    
    public static final int NOP = 0;
    public static final int INSERT = 1;
    public static final int UPDATE = 2;
    
    GsOrder go;
    
    public GsOrder() {
        initComponents();
        this.go = go;
        conn = MySQLConnect.connectDB();
        fillComboMJID();
        fillComboName();
        String sql = "SELECT * FROM gs_order";
        updateTableData(sql);
        disableAll();
        txtSearch.getDocument().addDocumentListener(new DocumentListener(){
           @Override
           public void insertUpdate(DocumentEvent e){
               searchFilter();
           }
           @Override
           public void removeUpdate(DocumentEvent e){
               searchFilter();
           }
           @Override
           public void changedUpdate(DocumentEvent e){
           }
           
        });
    }
    private static void addEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }
    private void fillComboMJID(){
        try{
            String sql = "select * from customer_order";
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            cbMJID.addItem("");
            while(rs.next()){
//                System.out.println(rs.getString("publication_id"));
                String mjid = rs.getString("mj_id");
                cbMJID.addItem(mjid);
            }
            cbMJID.addItemListener(new ItemListener() {
                @Override
                public void itemStateChanged(ItemEvent e) {
                    fillTaDesc();
                    filltxtW();
                    filltxtS();
                    fillImage();
                    fillRD();
                }
            });
        }catch(Exception e){
           JOptionPane.showMessageDialog(this, "Error! " + e); 
           e.printStackTrace();
        }
    }
    
    private void fillComboName(){
        try{
            String sql = "select * from goldsmith";
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            while(rs.next()){
                String name = rs.getString("name");
                cbName.addItem(name);
            }
        }catch(Exception e){
           JOptionPane.showMessageDialog(this, "Error! " + e); 
           e.printStackTrace();
        }
    }
    
    private void fillTaDesc(){
        String mjid = cbMJID.getSelectedItem().toString();
        try{
            String sql = "select * from customer_order where mj_id = '" + mjid + "'";
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            while(rs.next()){
                String desc = rs.getString("order_desc");              
                taOD.setText(desc);
            }
        }catch(Exception e){
           JOptionPane.showMessageDialog(this, "Error! " + e); 
           e.printStackTrace();
        }
    }
    private void filltxtW(){
        String mjid = cbMJID.getSelectedItem().toString();
        try{
            String sql = "select * from customer_order where mj_id = '" + mjid + "'";
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            while(rs.next()){
                String w = rs.getString("weight");              
                txtW.setText(w);
            }
        }catch(Exception e){
           JOptionPane.showMessageDialog(this, "Error! " + e); 
           e.printStackTrace();
        }
    }
    private void filltxtS(){
        String mjid = cbMJID.getSelectedItem().toString();
        try{
            String sql = "select * from customer_order where mj_id = '" + mjid + "'";
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            while(rs.next()){
                String s = rs.getString("size");              
                txtS.setText(s);
            }
        }catch(Exception e){
           JOptionPane.showMessageDialog(this, "Error! " + e); 
           e.printStackTrace();
        }
    }
    private void fillImage(){
        String mjid = cbMJID.getSelectedItem().toString();
        try{
            String sql = "select * from customer_order where mj_id = '" + mjid + "'";
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            while(rs.next()){
                byteImage = rs.getBytes("image");
                BufferedImage image = ImageIO.read(new ByteArrayInputStream(byteImage));
                ImageIcon icon = new ImageIcon(image);
                lblImage.setIcon(icon);
            }
        }catch(Exception e){
           JOptionPane.showMessageDialog(this, "Error! " + e); 
           e.printStackTrace();
        }
    }
    private void fillRD(){
        String mjid = cbMJID.getSelectedItem().toString();
        try{
            String sql = "SELECT * from customer_order where mj_id = '" + mjid + "'";
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
           
            while(rs.next()){
                String dateValue1 = rs.getString("delivery_date");
                LocalDate date = LocalDate.parse(dateValue1);
                LocalDate returnvalue = date.minusDays(2);
                dcRD.setText(returnvalue.toString());
                if(returnvalue.getDayOfWeek().toString() == "SUNDAY" || returnvalue.getDayOfWeek().toString() == "sunday"){
                    
                    LocalDate returnvalue1 = date.minusDays(3);
                    dcRD.setText(returnvalue1.toString());
                    
                }
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e); 
            e.printStackTrace();
        }
    }
    private DefaultTableModel getMyTableModel (TableModel dtm){
        
        int nRow = dtm.getRowCount(), nCol = dtm.getColumnCount();
        Object[][] tableData = new Object[nRow][nCol];
        for(int i=0; i<nRow; i++)
            for(int j=0; j<nCol; j++)
                tableData[i][j] = dtm.getValueAt(i, j);
        String[] colHeads = {"Order ID", "MJ ID","GS Name","Description","Weight","Size", "Given Date","Receive Date","Image", "Received"};
        DefaultTableModel myModel = new DefaultTableModel(tableData, colHeads){
            @Override
            public boolean isCellEditable(int row, int column){
                return false;
            }
        };
        return myModel;
    }
    
    
    private void enableAll(){
        txtW.setEnabled(true);
        rbtYes.setEnabled(true);
        rbtWrong.setEnabled(true);
        txtS.setEnabled(true);
        cbMJID.setEnabled(true);
        dcRD.setEnabled(true);
        cbName.setEnabled(true);
        dcGD.setEnabled(true);
        taOD.setEnabled(true);
    }
    
    private void disableAll(){
        txtW.setEnabled(false);
        rbtYes.setEnabled(false);
        rbtWrong.setEnabled(false);
        txtS.setEnabled(false);
        cbMJID.setEnabled(false);
        dcRD.setEnabled(false);
        cbName.setEnabled(false);
        dcGD.setEnabled(false);
        taOD.setEnabled(false);
    }
    
    private void searchFilter(){
        String key = txtSearch.getText();
        String sql;
        if(key.equals("")){
            sql = "SELECT * FROM gs_order";
        }else{
            sql = "SELECT * FROM gs_order WHERE gs_name like '%"+key+"%' OR mj_id like '%"+key+"%'";
        }
        updateTableData(sql);
    }
    
    private void insertRecord(){
        String sql = "INSERT INTO gs_order(mj_id, gs_name, order_desc, weight, size, given_date, receive_date, image, received) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        String mjid = cbMJID.getSelectedItem().toString();
        String name = cbName.getSelectedItem().toString();
        String desc = taOD.getText();
        String w = txtW.getText();
        String s = txtS.getText();
        Date selectedDate = dcGD.getDate();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = sdf.format(selectedDate);
        String rd = dcRD.getText();
        
        try {
            ps = conn.prepareStatement(sql);
            ps.setString(1, mjid);
            ps.setString(2, name);
            ps.setString(3, desc);
            ps.setString(4, w);
            ps.setString(5, s);
            ps.setString(6, formattedDate);
            ps.setString(7, rd);
            ps.setBytes(8, byteImage);
            ps.setString(9, "No");
            
            ps.execute();
//            clearFields();
            String loadTableQuery = "SELECT * FROM gs_order";
            updateTableData(loadTableQuery);
            JOptionPane.showMessageDialog(this, "Record Inserted Successfully!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(this, "Error while Inserting Record!" + ex.getMessage());
        }
    }
    private void updateRecord(){
        
        if(!"".equals(cbMJID.getSelectedItem().toString())){
            String sql = "UPDATE gs_order SET received = ? WHERE mj_id = ?";
            
            String mjid = cbMJID.getSelectedItem().toString();

            try {
                ps = conn.prepareStatement(sql);
     
                ps.setString(1, received);
                ps.setString(2, mjid);

                ps.execute();
//                clearFields();
                String loadTableQuery = "SELECT * FROM gs_order";
                updateTableData(loadTableQuery);
                JOptionPane.showMessageDialog(this, "Record Updated Successfully!");
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(this, "Error while Updating Record!" + ex.getMessage());
            }
       }
    }
    
    private void updateTableData(String sql){
        try{
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            tblGO.setModel(getMyTableModel(DbUtils.resultSetToTableModel(rs)));
        }catch(SQLException e){
            JOptionPane.showMessageDialog(this, "Error While Fetching Data from Database!");
        }
//        finally{
//            try{
//                rs.close();
//                ps.close();
//            }catch(SQLException ex){
//                JOptionPane.showMessageDialog(this, "Error While Closing ps or rs!");
//            }
//        }
    }
    
    private void clearFields(){
        txtW.setText("");
        txtS.setText("");
        
        rbtTemp.setSelected(true);
        received = null;
        
//        cbMJID.setSelectedIndex(-1);
        cbName.setSelectedIndex(-1);
        
        dcGD.setDate(null);
        taOD.setText("");
        lblImage.setIcon(null);
        dcRD.setText("");
    }
    
    private void deleteRecord(){
        String Yes = "Yes";
        String sql = "DELETE FROM gs_order WHERE received = '" + Yes + "'";
        try {
            ps = conn.prepareStatement(sql);
            ps.execute();
            sql = "SELECT * FROM gs_order";
            updateTableData(sql);
            clearFields();
            btnNew.setEnabled(true);
            btnClear.setEnabled(false);
            btnUpdate.setEnabled(false);
        }catch (SQLException ex) {
            JOptionPane.showMessageDialog(this, "Error While Deleting this record!");
        }
    }
    
    private void createPDF(){
        String mjid = cbMJID.getSelectedItem().toString();
        String fileName = "Goldsmith Order " + mjid;
//        String FILE = "C:/Users/Rasika Sadanand/Desktop/Orders/" + fileName + ".pdf";
        String FILE = "C:/Users/yashm/Desktop/" + fileName + ".pdf";
//        String FILE = "C:/Users/user/Desktop/Orders/" + fileName + ".pdf";
        Font headingFont = new Font(Font.FontFamily.TIMES_ROMAN, 18,Font.BOLD);
        Font subHeadingFont = new Font(Font.FontFamily.TIMES_ROMAN, 12,Font.BOLD);
        
        
        String name = cbName.getSelectedItem().toString();
        String desc = taOD.getText();
        String w = txtW.getText();
        String s = txtS.getText();
        Date selectedDate = dcGD.getDate();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = sdf.format(selectedDate);
        String rd = dcRD.getText();
        
        
        
        try {
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(FILE));
            document.open();
//            Image img = Image.getInstance("C:\\Users\\user\\Desktop\\Order Management System\\logo.jpg");
//            Image img = Image.getInstance("C:\\Users\\Rasika Sadanand\\Desktop\\Order Management System\\logo.jpg");
            Image img = Image.getInstance("C:\\Users\\yashm\\Documents\\Degree\\Java\\Netbeans Project\\OrderManagement\\src\\logo.jpg");
            img.scaleAbsolute(50f, 50f);
            img.setAlignment(Image.ALIGN_CENTER);
            document.add(img);
            document.add(new Paragraph("                                              M O D E R N", headingFont));
            Paragraph preface = new Paragraph();
            addEmptyLine(preface, 1);
            preface.add(new Paragraph("                                           Goldsmith Order", headingFont));
            addEmptyLine(preface, 1);
            preface.add(new Paragraph("MJ ID : " + mjid,subHeadingFont));
            preface.add(new Paragraph("Goldsmith Name : " + name,subHeadingFont));
            preface.add(new Paragraph("Order Description : " + desc,subHeadingFont));
            preface.add(new Paragraph("Weight : " + w,subHeadingFont));
            preface.add(new Paragraph("Size : " + s,subHeadingFont));
            preface.add(new Paragraph("Given Date : " + formattedDate,subHeadingFont));
            preface.add(new Paragraph("Receive Date : " + rd,subHeadingFont));
            preface.add(new Paragraph("Order Image : ", subHeadingFont));
            Image img1 = Image.getInstance(byteImage);
            img1.setAlignment(Image.ALIGN_CENTER);
            preface.add(img1);
            document.add(preface);
            document.close();
            clearFields();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    
    
    
    
    
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        rbtTemp = new javax.swing.JRadioButton();
        buttonGroup1 = new javax.swing.ButtonGroup();
        jLabel2 = new javax.swing.JLabel();
        txtOID = new javax.swing.JTextField();
        pnlBase = new javax.swing.JPanel();
        pnlTable = new javax.swing.JPanel();
        spGO = new javax.swing.JScrollPane();
        tblGO = new javax.swing.JTable();
        pnlDetails = new javax.swing.JPanel();
        lblMJID = new javax.swing.JLabel();
        lblOD = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        taOD = new javax.swing.JTextArea();
        lblGD = new javax.swing.JLabel();
        lblRD = new javax.swing.JLabel();
        dcGD = new com.toedter.calendar.JDateChooser();
        cbMJID = new javax.swing.JComboBox<>();
        jLabel1 = new javax.swing.JLabel();
        cbName = new javax.swing.JComboBox<>();
        jLabel3 = new javax.swing.JLabel();
        rbtYes = new javax.swing.JRadioButton();
        rbtWrong = new javax.swing.JRadioButton();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jDesktopPane1 = new javax.swing.JDesktopPane();
        lblImage = new javax.swing.JLabel();
        txtW = new javax.swing.JTextField();
        txtS = new javax.swing.JTextField();
        dcRD = new javax.swing.JTextField();
        pnlButtons = new javax.swing.JPanel();
        lblSearch = new javax.swing.JLabel();
        txtSearch = new javax.swing.JTextField();
        btnNew = new javax.swing.JButton();
        btnSave = new javax.swing.JButton();
        btnUpdate = new javax.swing.JButton();
        btnClear = new javax.swing.JButton();
        btnPrint = new javax.swing.JButton();

        buttonGroup1.add(rbtTemp);
        rbtTemp.setText("jRadioButton3");

        jLabel2.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        jLabel2.setText("Order ID");

        setBackground(new java.awt.Color(255, 255, 255));

        pnlBase.setBackground(new java.awt.Color(255, 255, 255));
        pnlBase.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Goldsmith Order", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Georgia", 1, 24))); // NOI18N

        pnlTable.setBackground(new java.awt.Color(255, 255, 255));
        pnlTable.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        tblGO.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        tblGO.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblGOMouseClicked(evt);
            }
        });
        spGO.setViewportView(tblGO);

        javax.swing.GroupLayout pnlTableLayout = new javax.swing.GroupLayout(pnlTable);
        pnlTable.setLayout(pnlTableLayout);
        pnlTableLayout.setHorizontalGroup(
            pnlTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTableLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(spGO)
                .addContainerGap())
        );
        pnlTableLayout.setVerticalGroup(
            pnlTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlTableLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(spGO, javax.swing.GroupLayout.DEFAULT_SIZE, 165, Short.MAX_VALUE)
                .addContainerGap())
        );

        pnlDetails.setBackground(new java.awt.Color(255, 255, 255));
        pnlDetails.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        lblMJID.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        lblMJID.setText("MJ ID");

        lblOD.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        lblOD.setText("Order Description");

        taOD.setColumns(20);
        taOD.setFont(new java.awt.Font("Georgia", 0, 18)); // NOI18N
        taOD.setRows(5);
        jScrollPane2.setViewportView(taOD);

        lblGD.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        lblGD.setText("Given Date");

        lblRD.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        lblRD.setText("Receive Date");

        cbMJID.setFont(new java.awt.Font("Georgia", 0, 18)); // NOI18N

        jLabel1.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        jLabel1.setText("GS Name");

        cbName.setFont(new java.awt.Font("Georgia", 0, 18)); // NOI18N

        jLabel3.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        jLabel3.setText("Received");

        buttonGroup1.add(rbtYes);
        rbtYes.setFont(new java.awt.Font("Georgia", 0, 18)); // NOI18N
        rbtYes.setText("Yes");
        rbtYes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtYesActionPerformed(evt);
            }
        });

        buttonGroup1.add(rbtWrong);
        rbtWrong.setFont(new java.awt.Font("Georgia", 0, 18)); // NOI18N
        rbtWrong.setText("Wrong");
        rbtWrong.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtWrongActionPerformed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        jLabel4.setText("Weight");

        jLabel5.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        jLabel5.setText("Size");

        jLabel6.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        jLabel6.setText("Image");

        lblImage.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));

        jDesktopPane1.setLayer(lblImage, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout jDesktopPane1Layout = new javax.swing.GroupLayout(jDesktopPane1);
        jDesktopPane1.setLayout(jDesktopPane1Layout);
        jDesktopPane1Layout.setHorizontalGroup(
            jDesktopPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDesktopPane1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblImage, javax.swing.GroupLayout.DEFAULT_SIZE, 191, Short.MAX_VALUE)
                .addContainerGap())
        );
        jDesktopPane1Layout.setVerticalGroup(
            jDesktopPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDesktopPane1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblImage, javax.swing.GroupLayout.DEFAULT_SIZE, 219, Short.MAX_VALUE)
                .addContainerGap())
        );

        txtW.setFont(new java.awt.Font("Georgia", 0, 18)); // NOI18N

        txtS.setFont(new java.awt.Font("Georgia", 0, 18)); // NOI18N

        dcRD.setFont(new java.awt.Font("Georgia", 0, 18)); // NOI18N

        javax.swing.GroupLayout pnlDetailsLayout = new javax.swing.GroupLayout(pnlDetails);
        pnlDetails.setLayout(pnlDetailsLayout);
        pnlDetailsLayout.setHorizontalGroup(
            pnlDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDetailsLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addGroup(pnlDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblMJID)
                    .addGroup(pnlDetailsLayout.createSequentialGroup()
                        .addGroup(pnlDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblOD)
                            .addComponent(jLabel4)
                            .addComponent(lblGD)
                            .addComponent(jLabel6))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jDesktopPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(pnlDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(dcGD, javax.swing.GroupLayout.DEFAULT_SIZE, 215, Short.MAX_VALUE)
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                                .addComponent(cbMJID, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(txtW, javax.swing.GroupLayout.Alignment.TRAILING)))
                        .addGap(111, 111, 111)
                        .addGroup(pnlDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblRD)
                            .addComponent(jLabel1)
                            .addComponent(jLabel3)
                            .addComponent(jLabel5))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtS)
                            .addGroup(pnlDetailsLayout.createSequentialGroup()
                                .addGap(28, 28, 28)
                                .addComponent(rbtYes)
                                .addGap(18, 18, 18)
                                .addComponent(rbtWrong))
                            .addComponent(cbName, 0, 215, Short.MAX_VALUE)
                            .addComponent(dcRD))))
                .addContainerGap(41, Short.MAX_VALUE))
        );
        pnlDetailsLayout.setVerticalGroup(
            pnlDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlDetailsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblMJID)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(cbMJID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel1)
                        .addComponent(cbName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGroup(pnlDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlDetailsLayout.createSequentialGroup()
                        .addGap(56, 56, 56)
                        .addComponent(lblOD))
                    .addGroup(pnlDetailsLayout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(24, 24, 24)
                .addGroup(pnlDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(pnlDetailsLayout.createSequentialGroup()
                        .addGroup(pnlDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(jLabel5)
                            .addComponent(txtW, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtS, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(pnlDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(dcGD, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblGD)))
                    .addGroup(pnlDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblRD)
                        .addComponent(dcRD, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGroup(pnlDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlDetailsLayout.createSequentialGroup()
                        .addGap(147, 147, 147)
                        .addComponent(jLabel6))
                    .addGroup(pnlDetailsLayout.createSequentialGroup()
                        .addGap(24, 24, 24)
                        .addGroup(pnlDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jDesktopPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(pnlDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel3)
                                .addComponent(rbtYes)
                                .addComponent(rbtWrong)))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pnlButtons.setBackground(new java.awt.Color(255, 255, 255));
        pnlButtons.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        lblSearch.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        lblSearch.setText("Search");

        txtSearch.setFont(new java.awt.Font("Georgia", 0, 18)); // NOI18N

        btnNew.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        btnNew.setText("New");
        btnNew.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNewActionPerformed(evt);
            }
        });

        btnSave.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        btnSave.setText("Save");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        btnUpdate.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        btnUpdate.setText("Edit");
        btnUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateActionPerformed(evt);
            }
        });

        btnClear.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        btnClear.setText("Clear");
        btnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearActionPerformed(evt);
            }
        });

        btnPrint.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        btnPrint.setText("Print");
        btnPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrintActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlButtonsLayout = new javax.swing.GroupLayout(pnlButtons);
        pnlButtons.setLayout(pnlButtonsLayout);
        pnlButtonsLayout.setHorizontalGroup(
            pnlButtonsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlButtonsLayout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addComponent(lblSearch)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(82, 82, 82)
                .addComponent(btnNew)
                .addGap(18, 18, 18)
                .addComponent(btnSave)
                .addGap(18, 18, 18)
                .addComponent(btnUpdate)
                .addGap(18, 18, 18)
                .addComponent(btnClear)
                .addGap(18, 18, 18)
                .addComponent(btnPrint)
                .addContainerGap(65, Short.MAX_VALUE))
        );
        pnlButtonsLayout.setVerticalGroup(
            pnlButtonsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlButtonsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlButtonsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblSearch)
                    .addComponent(txtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnNew)
                    .addComponent(btnSave)
                    .addComponent(btnUpdate)
                    .addComponent(btnClear)
                    .addComponent(btnPrint))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout pnlBaseLayout = new javax.swing.GroupLayout(pnlBase);
        pnlBase.setLayout(pnlBaseLayout);
        pnlBaseLayout.setHorizontalGroup(
            pnlBaseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlTable, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(pnlDetails, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(pnlButtons, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        pnlBaseLayout.setVerticalGroup(
            pnlBaseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlBaseLayout.createSequentialGroup()
                .addComponent(pnlDetails, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlButtons, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlTable, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlBase, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlBase, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        // TODO add your handling code here:
        if(operation == INSERT){
            insertRecord();
            createPDF();
        }else if(operation == UPDATE){
            updateRecord();
            deleteRecord();
        }
        operation = NOP;
        btnClear.setEnabled(false);
        btnNew.setEnabled(true);
        btnSave.setEnabled(false);
        btnUpdate.setEnabled(false);
        clearFields();
    }//GEN-LAST:event_btnSaveActionPerformed

    private void tblGOMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblGOMouseClicked
        // TODO add your handling code here:

        int selectedRow = tblGO.getSelectedRow();
        String selectedOID = tblGO.getModel().getValueAt(selectedRow, 0).toString();
        try{
            String sql = "SELECT * FROM gs_order WHERE order_id = '" + selectedOID + "'";
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            
            if(rs.next()){
                cbName.setSelectedItem(rs.getString("gs_name"));
                
                String dateValue = rs.getString("given_date");
                Date date = new SimpleDateFormat("yyyy-MM-dd").parse(dateValue);
                dcGD.setDate(date);
                
                dcRD.setText(rs.getString("receive_date"));
                txtOID.setText(rs.getString("order_id"));
                cbMJID.setSelectedItem(rs.getString("mj_id"));
                
                disableAll();
                btnUpdate.setEnabled(true);
                btnNew.setEnabled(false);
                btnSave.setEnabled(false);
                btnClear.setEnabled(false);
                
            }
        }catch(SQLException e){
            JOptionPane.showMessageDialog(this, "Error: " + e);
        } catch (ParseException ex) {
             JOptionPane.showMessageDialog(this, "Error: " + ex);
        }
    }//GEN-LAST:event_tblGOMouseClicked

    private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearActionPerformed
        // TODO add your handling code here:
        clearFields();
    }//GEN-LAST:event_btnClearActionPerformed

    private void btnUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateActionPerformed
        // TODO add your handling code here:
        operation = UPDATE;
        disableAll();
        rbtYes.setEnabled(true);
        rbtWrong.setEnabled(true);
        btnNew.setEnabled(false);
        btnUpdate.setEnabled(false);
        btnSave.setEnabled(true);
        btnClear.setEnabled(false);
    }//GEN-LAST:event_btnUpdateActionPerformed

    private void btnNewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNewActionPerformed
        // TODO add your handling code here:
//          clearFields();
        enableAll();
        taOD.setEnabled(false);
        txtW.setEnabled(false);
        txtS.setEnabled(false);
        dcRD.setEnabled(false);
        
        txtOID.setEnabled(false);
        rbtYes.setEnabled(false);
        rbtWrong.setEnabled(false);
        btnNew.setEnabled(false);
        btnSave.setEnabled(true);
        btnClear.setEnabled(true);
        operation = INSERT;
    }//GEN-LAST:event_btnNewActionPerformed

    private void rbtYesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtYesActionPerformed
        // TODO add your handling code here:
        received = "Yes";
    }//GEN-LAST:event_rbtYesActionPerformed

    private void rbtWrongActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtWrongActionPerformed
        // TODO add your handling code here:
        received = "Wrong";
    }//GEN-LAST:event_rbtWrongActionPerformed

    private void btnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrintActionPerformed
        // TODO add your handling code here:
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");  
        LocalDateTime now = LocalDateTime.now();
        MessageFormat header =  new MessageFormat("Goldsmith Orders Date : " + dtf.format(now));
        MessageFormat footer =  new MessageFormat("Page{0, number, integer}");
        try{
            tblGO.print(JTable.PrintMode.FIT_WIDTH, header, footer);
        }catch(Exception e){
            System.err.format("Cannot print %s%n", e.getMessage());
        }
    }//GEN-LAST:event_btnPrintActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnClear;
    private javax.swing.JButton btnNew;
    private javax.swing.JButton btnPrint;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnUpdate;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JComboBox<String> cbMJID;
    private javax.swing.JComboBox<String> cbName;
    private com.toedter.calendar.JDateChooser dcGD;
    private javax.swing.JTextField dcRD;
    private javax.swing.JDesktopPane jDesktopPane1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblGD;
    private javax.swing.JLabel lblImage;
    private javax.swing.JLabel lblMJID;
    private javax.swing.JLabel lblOD;
    private javax.swing.JLabel lblRD;
    private javax.swing.JLabel lblSearch;
    private javax.swing.JPanel pnlBase;
    private javax.swing.JPanel pnlButtons;
    private javax.swing.JPanel pnlDetails;
    private javax.swing.JPanel pnlTable;
    private javax.swing.JRadioButton rbtTemp;
    private javax.swing.JRadioButton rbtWrong;
    private javax.swing.JRadioButton rbtYes;
    private javax.swing.JScrollPane spGO;
    private javax.swing.JTextArea taOD;
    private javax.swing.JTable tblGO;
    private javax.swing.JTextField txtOID;
    private javax.swing.JTextField txtS;
    private javax.swing.JTextField txtSearch;
    private javax.swing.JTextField txtW;
    // End of variables declaration//GEN-END:variables
    private String received = "";
    private byte[] byteImage;
    private int operation;
}
