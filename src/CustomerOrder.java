
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import net.coobird.thumbnailator.Thumbnails;
import net.proteanit.sql.DbUtils;
import java.io.FileOutputStream;
import java.util.Date;

import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chapter;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.List;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Section;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.text.MessageFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import javax.swing.JTable;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author yashm
 */
/*
Test Keys : 
    API Key : D0PV39F73X5AG3CT14TX9AW1TYNBVCIW
    Secret Key : D4TOEO3223O75P6H
Live Keys : 
    API Key : FGMRNBOSHZN4HOZFLEYI5PA9Y48H9MZ2
    Secret Key : LCT1UHJYW5RJHZ84
Name : Modern Jewellers
Number : 9820254233
Password : Rupal@123
*/
public class CustomerOrder extends javax.swing.JPanel {

    /**
     * Creates new form CustomerOrder
     */
    CustomerOrder co;
    
    Connection conn = null;
    Statement statement = null;
    PreparedStatement ps = null;
    ResultSet rs = null;
    
    public static final int NOP = 0;
    public static final int INSERT = 1;
    public static final int UPDATE = 2;
    
    
    
    public CustomerOrder() {
        initComponents();
        conn = MySQLConnect.connectDB();
        this.co = this;
        String sql = "SELECT customer_id, order_id, mj_id, order_desc, weight, size, receive_date, delivery_date FROM customer_order";
        updateTableData(sql);
        disableAll();
        txtSearch.getDocument().addDocumentListener(new DocumentListener(){
           @Override
           public void insertUpdate(DocumentEvent e){
               searchFilter();
           }
           @Override
           public void removeUpdate(DocumentEvent e){
               searchFilter();
           }
           @Override
           public void changedUpdate(DocumentEvent e){
           }
        });
        fillComboCID();
    }
    
    private static void addEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }
    
    private DefaultTableModel getMyTableModel (TableModel dtm){
        
        int nRow = dtm.getRowCount(), nCol = dtm.getColumnCount();
        Object[][] tableData = new Object[nRow][nCol];
        for(int i=0; i<nRow; i++)
            for(int j=0; j<nCol; j++)
                tableData[i][j] = dtm.getValueAt(i, j);
        String[] colHeads = {"CustomerID", "OrderID", "MJID", "Description", "Weight", "Size", "Receive Date", "Delivery Date"};
        DefaultTableModel myModel = new DefaultTableModel(tableData, colHeads){
            @Override
            public boolean isCellEditable(int row, int column){
                return false;
            }
        };
        return myModel;
    }
    private void fillComboCID(){
        try{
            String sql = "select * from customer";
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            
            while(rs.next()){
//                System.out.println(rs.getString("publication_id"));
                String cid = rs.getString("ID");
                cbCID.addItem(cid);
            }
        }catch(Exception e){
           JOptionPane.showMessageDialog(this, "Error! " + e); 
           e.printStackTrace();
        }
    }
    private void createMJID(){
        try{
            String sql = "SELECT MAX(CAST(SUBSTRING(mj_id, 2, LENGTH(mj_id)) AS INT)) FROM customer_order;";
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            
            if(rs.next()){
                int mjid = rs.getInt("MAX(CAST(SUBSTRING(mj_id, 2, LENGTH(mj_id)) AS INT))");
                mjid = mjid + 1;
                String finalMJID = "Z" + mjid;
                txtMJID.setText(finalMJID);
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Error! " + e); 
            e.printStackTrace();
        }  
    }
    
    
    
    private void enableAll(){
        cbCID.setEnabled(true);
        txtOID.setEnabled(true);
        txtMJID.setEnabled(true);
        taDesc.setEnabled(true);
        cmbW.setEnabled(true);
        cmbS.setEnabled(true);
        dcRD.setEnabled(true);
        dcDD.setEnabled(true);
        txtip.setEnabled(true); 
        btnUI.setEnabled(true);
    }
    
    private void disableAll(){
        cbCID.setEnabled(false);
        txtOID.setEnabled(false);
        txtMJID.setEnabled(false);
        taDesc.setEnabled(false);
        cmbW.setEnabled(false);
        cmbS.setEnabled(false);
        dcRD.setEnabled(false);
        dcDD.setEnabled(false);
        txtip.setEnabled(false);
        btnUI.setEnabled(false);
    }
    private void searchFilter(){
        String key = txtSearch.getText();
        String sql;
        if(key.equals("")){
            sql = "SELECT customer_id, order_id, mj_id, order_desc, weight, size, receive_date, delivery_date FROM customer_order";
        }else{
            sql = "SELECT customer_id, order_id, mj_id, order_desc, weight, size, receive_date, delivery_date FROM customer_order WHERE mj_id like '%"+key+"%' OR customer_id like '%"+key+"%'";
        }
        updateTableData(sql);
    }
    
    
    private void insertRecord(){
        String sql = "INSERT INTO customer_order(customer_id, order_id, mj_id, order_desc, weight, size, receive_date, delivery_date, image, expiry_date) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        String cid = cbCID.getSelectedItem().toString();
        String oid = txtOID.getText();
        String mjid = txtMJID.getText();
        String desc = taDesc.getText();
        String weight = cmbW.getSelectedItem().toString();
        String size = cmbS.getSelectedItem().toString();
        
        Date selectedDate = dcRD.getDate();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = sdf.format(selectedDate);
        
        Date selectedDate1 = dcDD.getDate();    
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate1 = sdf1.format(selectedDate1);
        
        LocalDate ed = LocalDate.parse(formattedDate1);
        LocalDate edr = ed.plusDays(60);
        String expiredDate = edr.toString();
        
        
        try {
            ps = conn.prepareStatement(sql);
            ps.setString(1, cid);
            ps.setString(2, oid);
            ps.setString(3, mjid);
            ps.setString(4, desc);
            ps.setString(5, weight);
            ps.setString(6, size);
            ps.setString(7, formattedDate);
            ps.setString(8, formattedDate1);
            ps.setBytes(9, byteImage);
            ps.setString(10, expiredDate);
            
            ps.execute();
//            clearFields();
            String loadTableQuery = "SELECT * FROM customer_order";
            updateTableData(loadTableQuery);
            JOptionPane.showMessageDialog(this, "Record Inserted Successfully!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(this, "Error while Inserting Record!" + ex.getMessage());
        }
    }
    
    private void updateRecord(){
        
        if(!"".equals(txtMJID.getText())){
            String sql = "UPDATE customer_order SET customer_id = ?, order_id = ?, order_desc = ?, weight = ?, size = ?, receive_date = ?, delivery_date = ?, image = ?, expiry_date = ? WHERE mj_id = ?";
            String cid = cbCID.getSelectedItem().toString();
            String oid = txtOID.getText();
            String mjid = txtMJID.getText();
            String desc = taDesc.getText();
            String weight = cmbW.getSelectedItem().toString();
            String size = cmbS.getSelectedItem().toString();

            Date selectedDate = dcRD.getDate();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String formattedDate = sdf.format(selectedDate);

            Date selectedDate1 = dcDD.getDate();    
            SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
            String formattedDate1 = sdf1.format(selectedDate1);

            try {
                ps = conn.prepareStatement(sql);
                ps.setString(1, cid);
                ps.setString(2, oid);
                ps.setString(3, desc);
                ps.setString(4, weight);
                ps.setString(5, size);
                ps.setString(6, formattedDate);
                ps.setString(7, formattedDate1);
                ps.setBytes(8, byteImage);
                ps.setString(9, null);
                ps.setString(10, mjid);

                ps.execute();
//                clearFields();
                String loadTableQuery = "SELECT * FROM customer_order";
                updateTableData(loadTableQuery);
                JOptionPane.showMessageDialog(this, "Record Updated Successfully!");
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(this, "Error while Updating Record!" + ex.getMessage());
            }
       }
    }
    private void updateTableData(String sql){
        try{
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            tblCO.setModel(getMyTableModel(DbUtils.resultSetToTableModel(rs)));
        }catch(SQLException e){
            JOptionPane.showMessageDialog(this, "Error While Fetching Data from Database!");
        }finally{
            try{
                rs.close();
                ps.close();
            }catch(SQLException ex){
                JOptionPane.showMessageDialog(this, "Error While Closing ps or rs!");
            }
        }
    }
    
    
    
    
    
    private void clearFields(){
        cbCID.setSelectedIndex(-1);
        txtOID.setText("");
        txtMJID.setText("");    
        taDesc.setText("");
        cmbW.setSelectedIndex(-1);
        cmbS.setSelectedIndex(-1);
        dcRD.setDate(null);
        dcDD.setDate(null);
        txtip.setText("");
        lblImage.setIcon(null);
    }
    
    private void createPDF(){
        String mjid = txtMJID.getText();
        String fileName = "Customer Order " + mjid;
//        String FILE = "C:/Users/Rasika Sadanand/Desktop/Orders/" + fileName + ".pdf";
        String FILE = "C:/Users/yashm/Desktop/" + fileName + ".pdf";
//        String FILE = "C:/Users/user/Desktop/Orders/" + fileName + ".pdf";
        Font headingFont = new Font(Font.FontFamily.TIMES_ROMAN, 18,Font.BOLD);
        Font subHeadingFont = new Font(Font.FontFamily.TIMES_ROMAN, 12,Font.BOLD);
        
        String cid = cbCID.getSelectedItem().toString();
        String oid = txtOID.getText();
        
        String desc = taDesc.getText();
        String weight = cmbW.getSelectedItem().toString();
        String size = cmbS.getSelectedItem().toString();
        
        Date selectedDate = dcRD.getDate();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = sdf.format(selectedDate);
        
        Date selectedDate1 = dcDD.getDate();    
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate1 = sdf1.format(selectedDate1);
        
        
        
        try {
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(FILE));
            document.open();
//            Image img = Image.getInstance("C:\\Users\\Rasika Sadanand\\Desktop\\Order Management System\\logo.jpg");
            Image img = Image.getInstance("C:\\Users\\yashm\\Documents\\Degree\\Java\\Netbeans Project\\OrderManagement\\src\\logo.jpg");
//            Image img = Image.getInstance("C:\\Users\\user\\Desktop\\Order Management System\\logo.jpg");
            img.scaleAbsolute(50f, 50f);
            img.setAlignment(Image.ALIGN_CENTER);
            document.add(img);
            document.add(new Paragraph("                                              M O D E R N", headingFont));
            Paragraph preface = new Paragraph();
            addEmptyLine(preface, 1);
            preface.add(new Paragraph("                                           Customer Order", headingFont));
            addEmptyLine(preface, 1);
            preface.add(new Paragraph("Customer Name : " + cid,subHeadingFont));
            preface.add(new Paragraph("Customer Order ID : " + oid,subHeadingFont));
            preface.add(new Paragraph("MJ ID : " + mjid,subHeadingFont));
            preface.add(new Paragraph("Order Description : " + desc,subHeadingFont));
            preface.add(new Paragraph("Weight : " + weight,subHeadingFont));
            preface.add(new Paragraph("Size : " + size,subHeadingFont));
            preface.add(new Paragraph("Receive Date : " + formattedDate,subHeadingFont));
            preface.add(new Paragraph("Customer Delivery Date : " + formattedDate1,subHeadingFont));
            preface.add(new Paragraph("Order Image : ", subHeadingFont));
            Image img1 = Image.getInstance(byteImage);
            img1.setAlignment(Image.ALIGN_CENTER);
            preface.add(img1);
            document.add(preface);
            document.close();
            clearFields();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void createPDFUpdated(){
        String mjid = txtMJID.getText();
        String fileName = "Customer Order " + mjid + " Updated";
//        String FILE = "C:/Users/Rasika Sadanand/Desktop/Orders/" + fileName + ".pdf";
        String FILE = "C:/Users/yashm/Desktop/" + fileName + ".pdf";
//        String FILE = "C:/Users/user/Desktop/Orders/" + fileName + ".pdf";
        Font headingFont = new Font(Font.FontFamily.TIMES_ROMAN, 18,Font.BOLD);
        Font subHeadingFont = new Font(Font.FontFamily.TIMES_ROMAN, 12,Font.BOLD);
        
        String cid = cbCID.getSelectedItem().toString();
        String oid = txtOID.getText();
        
        String desc = taDesc.getText();
        String weight = cmbW.getSelectedItem().toString();
        String size = cmbS.getSelectedItem().toString();
        
        Date selectedDate = dcRD.getDate();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = sdf.format(selectedDate);
        
        Date selectedDate1 = dcDD.getDate();    
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate1 = sdf1.format(selectedDate1);
        
        
        
        try {
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(FILE));
            document.open();
//            Image img = Image.getInstance("C:\\Users\\Rasika Sadanand\\Desktop\\Order Management System\\logo.jpg");
            Image img = Image.getInstance("C:\\Users\\yashm\\Documents\\Degree\\Java\\Netbeans Project\\OrderManagement\\src\\logo.jpg");
//            Image img = Image.getInstance("C:\\Users\\user\\Desktop\\Order Management System\\logo.jpg");
            img.scaleAbsolute(50f, 50f);
            img.setAlignment(Image.ALIGN_CENTER);
            document.add(img);
            document.add(new Paragraph("                                              M O D E R N", headingFont));
            Paragraph preface = new Paragraph();
            addEmptyLine(preface, 1);
            preface.add(new Paragraph("                                           Customer Order", headingFont));
            addEmptyLine(preface, 1);
            preface.add(new Paragraph("Customer Name : " + cid,subHeadingFont));
            preface.add(new Paragraph("Customer Order ID : " + oid,subHeadingFont));
            preface.add(new Paragraph("MJ ID : " + mjid,subHeadingFont));
            preface.add(new Paragraph("Order Description : " + desc,subHeadingFont));
            preface.add(new Paragraph("Weight : " + weight,subHeadingFont));
            preface.add(new Paragraph("Size : " + size,subHeadingFont));
            preface.add(new Paragraph("Receive Date : " + formattedDate,subHeadingFont));
            preface.add(new Paragraph("Customer Delivery Date : " + formattedDate1,subHeadingFont));
            preface.add(new Paragraph("Order Image : ", subHeadingFont));
            Image img1 = Image.getInstance(byteImage);
            img1.setAlignment(Image.ALIGN_CENTER);
            preface.add(img1);
            document.add(preface);
            document.close();
            clearFields();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        rbTemp = new javax.swing.JRadioButton();
        rbtGroup = new javax.swing.ButtonGroup();
        rbtTemp1 = new javax.swing.JRadioButton();
        rbtGroup1 = new javax.swing.ButtonGroup();
        pnlBase = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        cbCID = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();
        txtOID = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtMJID = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        dcDD = new com.toedter.calendar.JDateChooser();
        jLabel5 = new javax.swing.JLabel();
        spDesc = new javax.swing.JScrollPane();
        taDesc = new javax.swing.JTextArea();
        dcRD = new com.toedter.calendar.JDateChooser();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        cmbW = new javax.swing.JComboBox<>();
        cmbS = new javax.swing.JComboBox<>();
        jDesktopPane1 = new javax.swing.JDesktopPane();
        lblImage = new javax.swing.JLabel();
        lblIP = new javax.swing.JLabel();
        txtip = new javax.swing.JTextField();
        btnUI = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        btnSave = new javax.swing.JButton();
        btnNew = new javax.swing.JButton();
        btnClear = new javax.swing.JButton();
        btnEdit = new javax.swing.JButton();
        txtSearch = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblCO = new javax.swing.JTable();

        rbtGroup.add(rbTemp);
        rbTemp.setText("jRadioButton1");

        rbtGroup1.add(rbtTemp1);
        rbtTemp1.setText("jRadioButton1");

        setBackground(new java.awt.Color(255, 255, 255));

        pnlBase.setBackground(new java.awt.Color(255, 255, 255));
        pnlBase.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Customer Order", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Georgia", 1, 24))); // NOI18N

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel1.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        jLabel1.setText("Customer Name");

        cbCID.setFont(new java.awt.Font("Georgia", 0, 18)); // NOI18N

        jLabel2.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        jLabel2.setText("Customer Order ID");

        txtOID.setFont(new java.awt.Font("Georgia", 0, 18)); // NOI18N

        jLabel3.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        jLabel3.setText("MJ ID");

        txtMJID.setFont(new java.awt.Font("Georgia", 0, 18)); // NOI18N

        jLabel6.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        jLabel6.setText("Receive Date");

        jLabel7.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        jLabel7.setText("Customer Delivery Date");

        jLabel5.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        jLabel5.setText("Description");

        taDesc.setColumns(20);
        taDesc.setFont(new java.awt.Font("Georgia", 0, 18)); // NOI18N
        taDesc.setRows(5);
        spDesc.setViewportView(taDesc);

        jLabel9.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        jLabel9.setText("Weight");

        jLabel10.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        jLabel10.setText("Size");

        jLabel11.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        jLabel11.setText("Image");

        cmbW.setFont(new java.awt.Font("Georgia", 0, 18)); // NOI18N
        cmbW.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Not Available", "1 gram", "2 grams", "3 grams", "4 grams", "5 grams", "6 grams", "7 grams", "8 grams", "9 grams", "10 grams", "11 grams", "12 grams", "13 grams", "14 grams", "15 grams", "16 grams", "17 grams", "18 grams", "19 grams", "20 grams", "21 grams", "22 grams", "23 grams", "24 grams", "25 grams", "26 grams", "27 grams", "28 grams", "29 grams", "30 grams", "31 grams", "32 grams", "33 grams", "34 grams", "35 grams", "36 grams", "37 grams", "38 grams", "39 grams", "40 grams", "41 grams", "42 grams", "43 grams", "44 grams", "45 grams", "46 grams", "47 grams", "48 grams", "49 grams", "50 grams", "51 grams", "52 grams", "53 grams", "54 grams", "55 grams", "56 grams", "57 grams", "58 grams", "59 grams", "60 grams", "61 grams", "62 grams", "63 grams", "64 grams", "65 grams", "66 grams", "67 grams", "68 grams", "69 grams", "70 grams", "71 grams", "72 grams", "73 grams", "74 grams", "75 grams", "76 grams", "77 grams", "78 grams", "79 grams", "80 grams", "81 grams", "82 grams", "83 grams", "84 grams", "85 grams", "86 grams", "87 grams", "88 grams", "89 grams", "90 grams", "91 grams", "92 grams", "93 grams", "94 grams", "95 grams", "96 grams", "97 grams", "98 grams", "99 grams", "100 grams", "101 grams", "102 grams", "103 grams", "104 grams", "105 grams", "106 grams", "107 grams", "108 grams", "109 grams", "110 grams", "111 grams", "112 grams", "113 grams", "114 grams", "115 grams", "116 grams", "117 grams", "118 grams", "119 grams", "120 grams", "121 grams", "122 grams", "123 grams", "124 grams", "125 grams", "126 grams", "127 grams", "128 grams", "129 grams", "130 grams", "131 grams", "132 grams", "133 grams", "134 grams", "135 grams", "136 grams", "137 grams", "138 grams", "139 grams", "140 grams", "141 grams", "142 grams", "143 grams", "144 grams", "145 grams", "146 grams", "147 grams", "148 grams", "149 grams", "150 grams", "151 grams", "152 grams", "153 grams", "154 grams", "155 grams", "156 grams", "157 grams", "158 grams", "159 grams", "160 grams", "161 grams", "162 grams", "163 grams", "164 grams", "165 grams", "166 grams", "167 grams", "168 grams", "169 grams", "170 grams", "171 grams", "172 grams", "173 grams", "174 grams", "175 grams", "176 grams", "177 grams", "178 grams", "179 grams", "180 grams", "181 grams", "182 grams", "183 grams", "184 grams", "185 grams", "186 grams", "187 grams", "188 grams", "189 grams", "190 grams", "191 grams", "192 grams", "193 grams", "194 grams", "195 grams", "196 grams", "197 grams", "198 grams", "199 grams", "200 grams", "201 grams", "202 grams", "203 grams", "204 grams", "205 grams", "206 grams", "207 grams", "208 grams", "209 grams", "210 grams", "211 grams", "212 grams", "213 grams", "214 grams", "215 grams", "216 grams", "217 grams", "218 grams", "219 grams", "220 grams", "221 grams", "222 grams", "223 grams", "224 grams", "225 grams", "226 grams", "227 grams", "228 grams", "229 grams", "230 grams", "231 grams", "232 grams", "233 grams", "234 grams", "235 grams", "236 grams", "237 grams", "238 grams", "239 grams", "240 grams", "241 grams", "242 grams", "243 grams", "244 grams", "245 grams", "246 grams", "247 grams", "248 grams", "249 grams", "250 grams", "251 grams", "252 grams", "253 grams", "254 grams", "255 grams", "256 grams", "257 grams", "258 grams", "259 grams", "260 grams", "261 grams", "262 grams", "263 grams", "264 grams", "265 grams", "266 grams", "267 grams", "268 grams", "269 grams", "270 grams", "271 grams", "272 grams", "273 grams", "274 grams", "275 grams", "276 grams", "277 grams", "278 grams", "279 grams", "280 grams", "281 grams", "282 grams", "283 grams", "284 grams", "285 grams", "286 grams", "287 grams", "288 grams", "289 grams", "290 grams", "291 grams", "292 grams", "293 grams", "294 grams", "295 grams", "296 grams", "297 grams", "298 grams", "299 grams", "300 grams" }));

        cmbS.setFont(new java.awt.Font("Georgia", 0, 18)); // NOI18N
        cmbS.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Not Available", "1.0", "1.1", "1.2", "1.3", "1.4", "1.5", "1.6", "1.7", "1.8", "1.9", "1.10", "1.11", "1.12", "1.13", "1.14", "1.15", "2.0", "2.0.5", "2.1", "2.1.5", "2.2", "2.2.5", "2.3", "2.3.5", "2.4", "2.4.5", "2.5", "2.5.5", "2.6", "2.6.5", "2.7", "2.7.5", "2.8", "2.8.5", "2.9", "2.9.5", "2.10", "2.10.5", "2.11", "2.11.5", "2.12", "2.12.5", "2.13", "2.13.5", "2.14", "2.14.5", "2.15", "2.15.5", "3.0", "3.0.5", "3.1", "3.1.5", "3.2", "3.2.5", "3.3", "3.3.5", "3.4", "3.4.5", "3.5", "3.5.5", "3.6", "3.6.5", "3.7", "3.7.5", "3.8", "3.8.5", "3.9", "3.9.5", "3.10", "3.10.5", "3.11", "3.11.5", "3.12", "3.12.5", "3.13", "3.13.5", "3.14", "3.14.5", "3.15", "3.15.5", "1 Number", "2 Number", "3 Number", "4 Number", "5 Number", "6 Number", "7 Number", "8 Number", "9 Number", "10 Number", "11 Number", "12 Number", "13 Number", "14 Number", "15 Number", "16 Number", "17 Number", "18 Number", "19 Number", "20 Number", "21 Number", "22 Number", "23 Number", "24 Number", "25 Number", "26 Number", "27 Number", "28 Number", "29 Number", "30 Number", "31 Number", "32 Number", "33 Number", "34 Number", "35 Number", "36 Number", "37 Number", "38 Number", "39 Number", "40 Number", "10 Inches", "11 Inches", "12 Inches", "13 Inches", "14 Inches", "15 Inches", "16 Inches", "17 Inches", "18 Inches", "19 Inches", "20 Inches", "21 Inches", "22 Inches", "23 Inches", "24 Inches", "25 Inches", "26 Inches", "27 Inches", "28 Inches", "29 Inches", "30 Inches", "31 Inches", "32 Inches", "33 Inches", "34 Inches", "35 Inches", "36 Inches", "37 Inches", "38 Inches", "39 Inches", "40 Inches", "41 Inches", "42 Inches", "43 Inches", "44 Inches", "45 Inches", "46 Inches", "47 Inches", "48 Inches", "49 Inches", "50 Inches" }));

        lblImage.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));

        jDesktopPane1.setLayer(lblImage, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout jDesktopPane1Layout = new javax.swing.GroupLayout(jDesktopPane1);
        jDesktopPane1.setLayout(jDesktopPane1Layout);
        jDesktopPane1Layout.setHorizontalGroup(
            jDesktopPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDesktopPane1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblImage, javax.swing.GroupLayout.DEFAULT_SIZE, 191, Short.MAX_VALUE)
                .addContainerGap())
        );
        jDesktopPane1Layout.setVerticalGroup(
            jDesktopPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDesktopPane1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblImage, javax.swing.GroupLayout.DEFAULT_SIZE, 219, Short.MAX_VALUE)
                .addContainerGap())
        );

        lblIP.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        lblIP.setText("Image Path");

        txtip.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N

        btnUI.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        btnUI.setText("Upload Image");
        btnUI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUIActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel9)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(cmbW, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel6)
                            .addComponent(jLabel11))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(dcRD, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jDesktopPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel3))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtMJID, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cbCID, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(0, 45, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel10)
                        .addComponent(btnUI)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(lblIP)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(txtip, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel7)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(dcDD, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5)
                            .addComponent(jLabel2))
                        .addGap(46, 46, 46)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(spDesc, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtOID, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cmbS, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(25, 25, 25))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addComponent(txtOID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbCID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(71, 71, 71)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(txtMJID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addComponent(spDesc, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(28, 28, 28)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(jLabel10)
                    .addComponent(cmbW, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cmbS, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(31, 31, 31)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel7)
                    .addComponent(dcDD, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(dcRD, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel6)))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(77, 77, 77)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblIP)
                            .addComponent(txtip, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(53, 53, 53)
                        .addComponent(btnUI)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jDesktopPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(124, 124, 124)
                                .addComponent(jLabel11)))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        btnSave.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        btnSave.setText("Save");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        btnNew.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        btnNew.setText("New");
        btnNew.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNewActionPerformed(evt);
            }
        });

        btnClear.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        btnClear.setText("Clear");
        btnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearActionPerformed(evt);
            }
        });

        btnEdit.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        btnEdit.setText("Edit");
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });

        txtSearch.setFont(new java.awt.Font("Georgia", 0, 18)); // NOI18N

        jLabel4.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        jLabel4.setText("Search");

        jButton1.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        jButton1.setText("Print");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(34, 34, 34)
                .addComponent(btnNew)
                .addGap(18, 18, 18)
                .addComponent(btnSave)
                .addGap(18, 18, 18)
                .addComponent(btnEdit)
                .addGap(18, 18, 18)
                .addComponent(btnClear)
                .addGap(18, 18, 18)
                .addComponent(jButton1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnNew)
                    .addComponent(btnSave)
                    .addComponent(btnClear)
                    .addComponent(btnEdit)
                    .addComponent(txtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4)
                    .addComponent(jButton1))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        tblCO.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblCO.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblCOMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tblCO);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 176, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout pnlBaseLayout = new javax.swing.GroupLayout(pnlBase);
        pnlBase.setLayout(pnlBaseLayout);
        pnlBaseLayout.setHorizontalGroup(
            pnlBaseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        pnlBaseLayout.setVerticalGroup(
            pnlBaseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlBaseLayout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlBase, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlBase, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        // TODO add your handling code here:
        if(operation == INSERT){
            insertRecord();
            createPDF();
        }else if(operation == UPDATE){
            updateRecord();
            createPDFUpdated();
        }
        operation = NOP;
        btnClear.setEnabled(false);
        btnEdit.setEnabled(false);
        btnNew.setEnabled(true);
        btnSave.setEnabled(false);
    }//GEN-LAST:event_btnSaveActionPerformed

    private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearActionPerformed
        // TODO add your handling code here:
        clearFields();
    }//GEN-LAST:event_btnClearActionPerformed

    private void btnNewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNewActionPerformed
        // TODO add your handling code here:
        clearFields();
        createMJID();
        enableAll();
        txtMJID.setEnabled(false);
        btnNew.setEnabled(false);
        btnEdit.setEnabled(false);
        btnSave.setEnabled(true);
        btnClear.setEnabled(true);
        operation = INSERT;
    }//GEN-LAST:event_btnNewActionPerformed

    private void btnUIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUIActionPerformed
        // TODO add your handling code here:
        JFileChooser chooser = new JFileChooser();
        chooser.setFileFilter(new FileNameExtensionFilter("image", "jpg"));
        chooser.showOpenDialog(this);
        File sourceFile = chooser.getSelectedFile();
        if(sourceFile != null){
            String sourceFilePath = sourceFile.getAbsolutePath();
            txtip.setText(sourceFilePath);
            BufferedImage thumbnail;
            try {
                thumbnail = Thumbnails.of(sourceFile).size(191, 219).asBufferedImage();
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                ImageIO.write(thumbnail, "jpg", baos);
                byteImage = baos.toByteArray();
                lblImage.setIcon(new ImageIcon(thumbnail));
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(null, "Image Error!" + ex);
            }
        }
    }//GEN-LAST:event_btnUIActionPerformed

    private void tblCOMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblCOMouseClicked
        // TODO add your handling code here:
        int selectedRow = tblCO.getSelectedRow();
        String selectedEmpID = tblCO.getModel().getValueAt(selectedRow, 2).toString();
        try{
            String sql = "SELECT * FROM customer_order WHERE mj_id = '" + selectedEmpID + "'";
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            
            if(rs.next()){
                cbCID.setSelectedItem(rs.getString("customer_id"));
                txtOID.setText(rs.getString("order_id"));
                txtMJID.setText(rs.getString("mj_id"));
                taDesc.setText(rs.getString("order_desc"));
                cmbW.setSelectedItem(rs.getString("weight"));
                cmbS.setSelectedItem(rs.getString("size"));
                
                String dateValue = rs.getString("receive_date");
                Date date = new SimpleDateFormat("yyyy-MM-dd").parse(dateValue);
                dcRD.setDate(date);
                String dateValue1 = rs.getString("delivery_date");
                Date date1 = new SimpleDateFormat("yyyy-MM-dd").parse(dateValue1);
                dcDD.setDate(date1);
           
                byteImage = rs.getBytes("image");
                BufferedImage image = ImageIO.read(new ByteArrayInputStream(byteImage));
                ImageIcon icon = new ImageIcon(image);
                lblImage.setIcon(icon);
                
                
                
                disableAll();
                btnEdit.setEnabled(true);
                btnNew.setEnabled(false);
                btnSave.setEnabled(false);
                btnClear.setEnabled(false);
                
            }
            
        }catch(SQLException e){
            JOptionPane.showMessageDialog(this, "SQL Error : " + e);
        } catch (ParseException ex) {
             JOptionPane.showMessageDialog(this, "Parse Error : " + ex);
        }catch(IOException e){
            JOptionPane.showMessageDialog(this, "IO Error : " + e);
        }
    }//GEN-LAST:event_tblCOMouseClicked

    private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
        // TODO add your handling code here:
        operation = UPDATE;
        enableAll();
        txtMJID.setEnabled(false);
        btnNew.setEnabled(false);
        btnEdit.setEnabled(false);
        btnSave.setEnabled(true);
        btnClear.setEnabled(false);
    }//GEN-LAST:event_btnEditActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");  
        LocalDateTime now = LocalDateTime.now();
        MessageFormat header =  new MessageFormat("Customer Orders Date : " + dtf.format(now));
        MessageFormat footer =  new MessageFormat("Page{0, number, integer}");
        try{
            tblCO.print(JTable.PrintMode.FIT_WIDTH, header, footer);
        }catch(Exception e){
            System.err.format("Cannot print %s%n", e.getMessage());
        }
    }//GEN-LAST:event_jButton1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnClear;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnNew;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnUI;
    private javax.swing.JComboBox<String> cbCID;
    private javax.swing.JComboBox<String> cmbS;
    private javax.swing.JComboBox<String> cmbW;
    private com.toedter.calendar.JDateChooser dcDD;
    private com.toedter.calendar.JDateChooser dcRD;
    private javax.swing.JButton jButton1;
    private javax.swing.JDesktopPane jDesktopPane1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblIP;
    private javax.swing.JLabel lblImage;
    private javax.swing.JPanel pnlBase;
    private javax.swing.JRadioButton rbTemp;
    private javax.swing.ButtonGroup rbtGroup;
    private javax.swing.ButtonGroup rbtGroup1;
    private javax.swing.JRadioButton rbtTemp1;
    private javax.swing.JScrollPane spDesc;
    private javax.swing.JTextArea taDesc;
    private javax.swing.JTable tblCO;
    private javax.swing.JTextField txtMJID;
    private javax.swing.JTextField txtOID;
    private javax.swing.JTextField txtSearch;
    private javax.swing.JTextField txtip;
    // End of variables declaration//GEN-END:variables
    private byte[] byteImage;
    private int operation;
}
